# tci-fe-test

## Steps to run the project
```
yarn global add json-server
json-server --watch db.json
yarn serve
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
