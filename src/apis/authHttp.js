import axios from 'axios'
import authHelper from '@/helper/auth'

const instance = axios.create({
  baseURL: '/api',
  timeout: 10000
})

instance.interceptors.request.use(config => {
  config.headers = {
    ...config.headers,
    token: authHelper.getToken()
  }

  return config
}, function (error) {
  return Promise.reject(error)
})

export default instance
