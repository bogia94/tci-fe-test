import authHttp from './authHttp'

export default {
  getPosts (params = {}) {
    const { page, limit, sort, order, title } = params

    return authHttp.get('/posts', {
      params: {
        title_like: title || undefined,
        _page: page,
        _limit: limit,
        _sort: sort,
        _order: order
      }
    })
  },
  getPost (id) {
    return authHttp.get(`/posts/${id}`)
      .then(res => res.data)
  },
  createPost (post) {
    return authHttp.post('/posts', post)
      .then(res => res.data)
  },
  updatePost (id, post) {
    return authHttp.put(`/posts/${id}`, post)
      .then(res => res.data)
  },
  deletePost (id) {
    return authHttp.delete(`/posts/${id}`)
      .then(res => res.data)
  }
}
