import { format } from 'date-fns'

export default function (value, pattern) {
  if (!value) return ''

  return format(value, pattern)
}
