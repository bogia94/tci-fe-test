const getToken = () => {
  return localStorage.getItem('token')
}

export default {
  getToken,
  isLoggedIn () {
    return !!getToken()
  },
  login (token) {
    return localStorage.setItem('token', token)
  },
  logout () {
    localStorage.clear()
  }
}
