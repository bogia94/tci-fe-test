import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import routerConfigurator from '@/router/routerConfigurator'
import dateFilter from '@/filters/date'
import store from '@/store'
import iView from 'iview'
import 'iview/dist/styles/iview.css'
import locale from 'iview/dist/locale/en-US'

Vue.config.productionTip = false

Vue.use(iView, { locale })
Vue.filter('date', dateFilter)

routerConfigurator.configAuthentication(router)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
