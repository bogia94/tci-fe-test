import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/dashboard',
      name: 'DashboardPage',
      meta: {
        requiresAuth: true
      },
      component: () => import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard.vue')
    },
    {
      path: '/posts',
      name: 'PostsPage',
      meta: {
        requiresAuth: true
      },
      component: () => import(/* webpackChunkName: "posts" */ '@/views/Posts.vue')
    },
    {
      path: '/posts/:id',
      name: 'EditPostPage',
      meta: {
        requiresAuth: true
      },
      props: true,
      component: () => import(/* webpackChunkName: "posts" */ '@/views/EditPost.vue')
    },
    {
      path: '/new-post',
      name: 'NewPostPage',
      meta: {
        requiresAuth: true
      },
      component: () => import(/* webpackChunkName: "posts" */ '@/views/NewPost.vue')
    },
    {
      path: '/login',
      name: 'LoginPage',
      component: () => import(/* webpackChunkName: "login" */ '@/views/Login.vue')
    },
    {
      path: '**',
      name: 'PageNotFound',
      component: () => import(/* webpackChunkName: "page-not-found" */ '@/views/PageNotFound.vue')
    }
  ]
})
