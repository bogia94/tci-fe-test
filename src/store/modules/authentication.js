import authenticationApi from '@/apis/authentication'
import jwtDecode from 'jwt-decode'
import authHelper from '@/helper/auth'

export const SET_USER = '[Authentication] SET_USER'

export default {
  namespaced: true,
  state: {
    user: undefined
  },
  mutations: {
    [SET_USER] (state, user) {
      state.user = user
    }
  },
  actions: {
    login ({ commit }, credential) {
      return authenticationApi.getToken(credential)
        .then((token) => {
          if (token) {
            authHelper.login(token)
            commit(SET_USER, jwtDecode(token))
            return true
          } else {
            return false
          }
        })
    },
    logout () {

    }
  },
  getters: {}
}
