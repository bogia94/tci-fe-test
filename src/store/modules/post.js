import postsApi from '@/apis/posts'

export const SET_POSTS = '[Post] SET POSTS'
export const SET_TOTAL = '[Post] SET TOTAL'
export const CHANGE_QUERY_PAGE = '[Post] CHANGE QUERY PAGE'
export const CHANGE_QUERY_LIMIT = '[Post] CHANGE QUERY LIMIT'
export const CHANGE_QUERY_TITLE = '[Post] CHANGE QUERY TITLE'
export const RESET_STATE = '[Post] RESET STATE'

const initialState = () => ({
  posts: [],
  total: 0,
  query: {
    page: 1,
    limit: 10,
    title: ''
  }
})

export default {
  namespaced: true,

  state: initialState,

  mutations: {
    [SET_POSTS] (state, posts) {
      state.posts = posts
    },
    [SET_TOTAL] (state, total) {
      state.total = total
    },
    [CHANGE_QUERY_PAGE] (state, page) {
      state.query.page = page
    },
    [CHANGE_QUERY_LIMIT] (state, limit) {
      state.query.limit = limit
    },
    [CHANGE_QUERY_TITLE] (state, title) {
      state.query.title = title
    },
    [RESET_STATE] (state) {
      const s = initialState()
      Object.keys(s).forEach(key => {
        state[key] = s[key]
      })
    }
  },

  actions: {
    fetchPosts ({ state, commit }) {
      return postsApi.getPosts({
        ...state.query
      }).then((response) => {
        commit(SET_POSTS, response.data)
        commit(SET_TOTAL, parseInt(response.headers['x-total-count'], 10))
      })
    },
    deletePost ({ state, commit, dispatch }, id) {
      return postsApi.deletePost(id).then(() => {
        if (state.query.page > 1 && state.posts.length === 1) {
          commit(CHANGE_QUERY_PAGE, state.query.page - 1)
        }

        dispatch('fetchPosts')
      })
    },
    changeLimit ({ commit, dispatch }, limit) {
      commit(CHANGE_QUERY_PAGE, 1)
      commit(CHANGE_QUERY_LIMIT, limit)
      return dispatch('fetchPosts')
    },
    search ({ commit, dispatch }, keyword) {
      commit(CHANGE_QUERY_PAGE, 1)
      commit(CHANGE_QUERY_TITLE, keyword)
      return dispatch('fetchPosts')
    }
  }
}
